# kionctl

Kionctl is a CLI to optimize usage of Kion (formerly known as CloudTamer)
for faster usage without needing to go to the web interface.

## Example Configuration

Below, you can find an example of a typical configuration file. For normal uses, place this file in `~/.config/kionctl/config.yml`, or alternatively, `/etc/kionctl/config.yml` for system-wide configuration.

```yaml
---
url: "https://kion.example.com"
api_key: "key here"
```

See [example-config.yml](example-config.yml) for all available options.

[![asciicast](https://asciinema.org/a/kS1ztjtm1r6l3e6gnY4raoUUv.svg)](https://asciinema.org/a/kS1ztjtm1r6l3e6gnY4raoUUv)

## Usage

As a rule, everything you need for usage is available directly from the command line. The following section is also available by running `kionctl usage`.

### Connecting to an Account

There are two ways to connect to an account. The simplest is to use the interactive prompts.

Run the following, and you'll be prompted for the required account and role information.

```bash
 $ kionctl get-creds
```

Alternatively, you can use `kionctl` non-interactively. To use this method, start by running the `list-accounts` command to view find account numbers or role names, all available projects, accounts, and roles.

```bash
 $ kionctl list-accounts

 Project             Account                                               Role
 prod                aws-examplecorp-prod (123456789012)                   ct-examplecorp-developer
 prod                aws-examplecorp-prod (123456789012)                   ct-examplecorp-dba
 staging             aws-examplecorp-stg  (345678901234)                   ct-examplecorp-superadmin
 staging             aws-examplecorp-stg  (345678901234)                   ct-examplecorp-developer
 staging             aws-examplecorp-stg  (345678901234)                   ct-examplecorp-dba
 dev                 aws-examplecorp-dev  (678901234567)                   ct-examplecorp-superadmin
 dev                 aws-examplecorp-dev  (678901234567)                   ct-examplecorp-developer
 dev                 aws-examplecorp-dev  (678901234567)                   ct-examplecorp-dba
```

In the above example, we see that there are three accounts (`dev`, `staging`, `prod`) and each
account has 3 roles (`dev`, `dba`, `superadmin`), except `prod` which does not have a `superadmin`
role available to the current user.

Then, determine what account and role to connect as. The example below will get credentials
in the staging account as the `developer` role.

```bash
$ kionctl get-creds -a 345678901234 -r ct-examplecorp-developer
```

After running this command, you will receive IAM credentials in environment variable form,
which you can then use to connect to the specified AWS account.

### Fetching Account Details

Due to Kion often returning account details slowly, kionctl makes use of caching of the
account information.

If you expect to see new account information and don't, you can do one of two things:

Fetch info - this fetches the latest account, project, and role list from Kion, and puts it
in the cache. This command is also something that might be useful to schedule in a cron-job
to run in the background every few days, to ensure the cached account info is always up
to date.

```bash
$ kionctl fetch-info
```
Flushing the cache - this clears all the caches, but does not reload them. They will be
reloaded the next time they are needed, leading to a slower execution. Generally, you don't
want to do this unless all else fails, as other useful data is also cached, like commonly
used accounts, among other things.

```bash
$ kionctl flush-cache
```

## Installation

### Homebrew

Install with homebrew by running the following:

```shell
brew tap benyanke/tools https://gitlab.com/benyanke/homebrew-tools.git
brew install benyanke/tools/kionctl
```

### Direct Download

[Go to the release page](https://gitlab.com/benyanke/kionctl/-/releases), find the latest release NOT marked
"pre-release" and fetch the binary matching your platform and architecture.


## Contributing

Feel free to open an issue or submit a MR for review. Standard Go workflows and style choices should be taken whenever possible. Individual features should begin as issues, then create a branch from the issue interface which can be MR'd in.

Released code exists in git tags/gitlab releases, available on the [release page](https://gitlab.com/benyanke/kionctl/-/releases).

### Offline Development Mode

To develop offline without an API server available, simply set the environment variable `KIONCTL_OFFLINE_DEV`=`1`. This allows you to do effectively all development on existing functionality without connectivity or access to a Kion server.

```bash
export KIONCTL_OFFLINE_DEV=1

# normal development commands
go run ./...

# or
go build
./kionctl [...]
```

Setting this variable does the following:

 - Mocks all HTTP calls to the Kion API and returns mock data so no calls are sent to the Kion server

 - Switches the cache keys to use a different namespace so there is not consistency issues with cached vs HTTP call data

### Release Process

Release process should, as a rule, follow the steps outlined below:

1) Make an issue with the bug or feature being implemented
2) Use the create-branch-from-issue functionality in the issue to create a branch
3) Implement the functionality on the branch
4) Create a merge request for the issue, merge into `master`, assuming CI passes and functionality is complete.
5) Go to the [tags](https://gitlab.com/benyanke/kionctl/-/tags) page and create a tag following standard semver practices.
6) Once CI finishes, the CI will have created a) a [release](https://gitlab.com/benyanke/kionctl/-/releases) as well as an update to the [homebrew tap package](https://gitlab.com/benyanke/homebrew-tools). Do a quick manual validation on these, and test that everything is working as expected.

