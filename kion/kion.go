package kion

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"sort"
	"time"

	"gitlab.com/benyanke/kionctl/kion/api"

	"gitlab.com/benyanke/kionctl/cache"
)

type Account = api.Account
type AccessRole = api.AccessRole
type Project = api.Project
type ApiClient = api.Client
type Credential = api.Credential

// Kion Client
type Client struct {
	BaseUrl     string      // URL to the kion server
	ApiToken    string      // API key for the kion server
	ApiClient   ApiClient   // API client struct
	HttpTimeout int         // Number of seconds before outbound HTTP requests are dropped
	Debug       bool        // Add addiitonal logging
	Cache       cache.Cache // Cache client
	NoReadCache bool        // Disable cache reading, forcing everything to fetch from server
}

// Statistics struct
type Stats struct {
	Events []StatEvent // List of raw events
	// NOTE - these three feilds don't include every event, just the last X events, based on
	// the "maxCount" param passed to recalculateCommonStats() below
	EventCount                     int                       // Count of events
	EventCountByType               map[string]int            // Count of events, event type is map key
	EventCountByCredentialModeType map[string]int            // Count of events, event type is mode (cli, console, etc)
	EventCountByAccountRole        map[string]map[string]int // Count of events, event type is map key, account name is second key, role is third key

	// TODO - add calculation of stats from the raw events later
}

type StatEvent struct {
	Type           string    // Type of credential event (typically either CLI or Web session)
	AccountNumber  string    // Account number
	RoleName       string    // Role name
	CredentialMode string    // How the credentials were requested (cli, console, file)
	Time           time.Time // Time of the credential event
}

// Checks configuration of kionClient, returns a slice of error strings if errors exist.
// Empty slice return == no config errors
func (k *Client) ValidateConfig() []string {

	var errs []string

	if len(k.BaseUrl) == 0 {
		errs = append(errs, "Must set base url")
	}

	if len(k.ApiToken) == 0 {
		errs = append(errs, "Must set API key")
	}

	// TODO - add other validation, so far we're just ensuring fields aren't empty

	return errs

}

// Checks configuration, exits with code 2 if bad
func (k Client) ExitOnValidationErrors() {
	errors := k.ValidateConfig()

	if len(errors) == 0 {
		return
	}

	fmt.Println("\nConfig validation error:")
	for _, error := range errors {
		fmt.Println("  - " + error)
	}
	fmt.Print("\nCan not continue. Exiting\n\n")

	os.Exit(2)
}

// Sets up the API client and other needed bits
func (k *Client) Init() {

	k.ApiClient = api.Client{
		BaseUrl:     k.BaseUrl,
		ApiToken:    k.ApiToken,
		HttpTimeout: k.HttpTimeout,
		Debug:       k.Debug,
	}

}

// Returns an IAM credential for a role and account ID
// No need to cache, this function just exists to provide a consistent interface
func (k Client) GetCredential(accountNumber string, roleName string, credentialMode string) (Credential, error) {

	err := k.ValidateIamRoleAndAccountNumber(accountNumber, roleName)
	if err != nil {
		return Credential{}, err
	}

	go k.IncrementStatistics("get-creds", accountNumber, roleName, credentialMode)

	credential, err := k.ApiClient.GetCredential(accountNumber, roleName)
	return credential, err

}

// Internal helper function - checks if a string is in a slice
func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// Returns the stats struct
func (k Client) GetStatistics() (stats Stats) {

	statsOut := Stats{}

	// Get existing stats if they exist
	if k.Cache.ExistsString(cache.KEY_STATISTICS) {
		statsRaw, _ := k.Cache.GetString(cache.KEY_STATISTICS)
		err := json.Unmarshal([]byte(statsRaw), &statsOut)

		// If the json is corrupted, just throw it out and start fresh
		if err != nil {
			statsOut = Stats{}
			if k.Debug {
				fmt.Println(err)
			}
		}
	}
	return statsOut

}

// Increment access logs/statistics for future common suggestions and auto complete
// You can run this asyncronously, like this:
//
//	go k.IncrementStatistics("get-creds", "123", "devops-admin")
func (k Client) IncrementStatistics(eventType string, accountNumber string, roleName string, credentialMode string) {

	validEventTypes := []string{"get-creds", "cache-flush"}

	if !stringInSlice(eventType, validEventTypes) {
		// Using a panic instead of error return to allow easier async usage
		panic("Invalid request type, try again")
	}

	stats := Stats{}

	// Get existing stats if they exist
	if k.Cache.ExistsString(cache.KEY_STATISTICS) {
		statsRaw, _ := k.Cache.GetString(cache.KEY_STATISTICS)
		err := json.Unmarshal([]byte(statsRaw), &stats)

		// If the json is corrupted, just throw it out and start fresh
		if err != nil {
			stats = Stats{}
			if k.Debug {
				fmt.Println("Invalid json in stats parser, emptying datastructure")
				fmt.Println(err)
			}
		}
	}

	// Add new event to slice of events
	stats.Events = append(stats.Events, StatEvent{
		Type:           eventType,
		AccountNumber:  accountNumber,
		RoleName:       roleName,
		CredentialMode: credentialMode,
		Time:           time.Now(),
	})

	// Calculate stats for the last 100 requests
	k.recalculateCommonStats(&stats, 100)

	// Convert back to json
	statsOutRaw, _ := json.Marshal(stats)

	// Store back in cache
	k.Cache.SetString(cache.KEY_STATISTICS, string(statsOutRaw))

}

// calculates stats from the raw list of stat events
func (k Client) recalculateCommonStats(stats *Stats, maxCount int) {
	count := 0
	countByCredMode := make(map[string]int)
	countByType := make(map[string]int)
	countByTypeAccountRole := make(map[string]map[string]int)

	// Put the newest events at the top
	sort.Slice(stats.Events, func(i, j int) bool {
		return stats.Events[i].Time.After(stats.Events[j].Time)
	})

	for _, event := range stats.Events {
		count++
		countByType[event.Type]++

		if len(event.AccountNumber) > 0 && len(event.RoleName) > 0 {
			if countByTypeAccountRole[event.AccountNumber] == nil {
				countByTypeAccountRole[event.AccountNumber] = make(map[string]int)
			}
			countByTypeAccountRole[event.AccountNumber][event.RoleName]++
			countByCredMode[event.CredentialMode]++
		}

		// Only consider the last $maxCount entries, not all of them - once we reach our limit, exit the loop
		if count >= maxCount {
			break
		}
	}
	stats.EventCount = count
	stats.EventCountByType = countByType
	stats.EventCountByAccountRole = countByTypeAccountRole
	stats.EventCountByCredentialModeType = countByCredMode

}

// Return the number of times an account/role combination has been recently used, based on stats
func (k Client) LookupStatsCountByAccountAndRole(account Account, role AccessRole) int {

	statsData := k.GetStatistics()
	for stat_account, stat_roles := range statsData.EventCountByAccountRole {
		if stat_account == account.Number {
			for stat_role, stat_count := range stat_roles {
				if stat_role == role.AwsIamRoleName {
					return stat_count
				}
			}
		}
	}
	// Return 0 if not found
	return 0
}

// Sorts the full project,account,role list, putting each slice in order to put the most used ones at the top
// Mostly used for user interface bits to surface commonly used options near the top
// Note that actual stats are stored in the roles, nested in accounts, which are nested in projects. The stats
// on accounts are the sum of all the stats on that account's roles, and likewise, the stats on projects are
// the sum of all the stats on that project's accounts
func (k Client) sortAccountInformationByStats(projects []Project) []Project {

	// First pass, add stats to struct fields (StatsCounter) at all levels
	// Very gross loop, but simply looping over each project, account, role,
	// and them looking up stats for each role
	for pi := 0; pi < len(projects); pi++ {
		for ai := 0; ai < len(projects[pi].Accounts); ai++ {
			for ri := 0; ri < len(projects[pi].Accounts[ai].AccessRoles); ri++ {

				count := k.LookupStatsCountByAccountAndRole(projects[pi].Accounts[ai], projects[pi].Accounts[ai].AccessRoles[ri])
				projects[pi].Accounts[ai].AccessRoles[ri].StatsCounter = count
				projects[pi].Accounts[ai].StatsCounter += count
				projects[pi].StatsCounter += count

			}
		}
	}

	// Second pass, sort each access role slice by stats
	for pi := 0; pi < len(projects); pi++ {
		for ai := 0; ai < len(projects[pi].Accounts); ai++ {
			sort.Slice(projects[pi].Accounts[ai].AccessRoles, func(i, j int) bool {
				return projects[pi].Accounts[ai].AccessRoles[i].StatsCounter > projects[pi].Accounts[ai].AccessRoles[j].StatsCounter
			})
		}
	}

	// Third pass, sort each account slice by stats
	for pi := 0; pi < len(projects); pi++ {
		sort.Slice(projects[pi].Accounts, func(i, j int) bool {
			return projects[pi].Accounts[i].StatsCounter > projects[pi].Accounts[j].StatsCounter
		})
	}

	// Fourth pass, sort each project slice by stats
	sort.Slice(projects, func(i, j int) bool {
		return projects[i].StatsCounter > projects[j].StatsCounter
	})

	return projects
}

// Returns null errors if the iam role and account number is a known one listed for the current user
// Returns human-readable error if iam role and/or account are invalid
func (k Client) ValidateIamRoleAndAccountNumber(accountNumber string, iamRole string) error {

	projects, err := k.GetAllAccountInformation()
	if err != nil {
		return err
	}

	accountFound := false
	// roleFound is implicit, because:
	// 1) you can't look for roles until you look for accounts, because accounts contain roles
	// therefore:
	// 2) if you find the role, it means you've also found the account, and can immediately exit with success

	// Loop over projects, accounts in the project, and roles in the
	// account, and print every possible combination in the table
	for _, project := range projects {
		for _, account := range project.Accounts {
			if account.Number == accountNumber {
				accountFound = true
				for _, role := range account.AccessRoles {
					if role.AwsIamRoleName == iamRole {
						return nil
					}
				}
				// No need to loop through rest of accounts if we get here
				continue
			}
		}
	}

	commonPostfix := "\nUse the 'list-accounts' command to view accounts and roles.\nIf you expect to see the role, run the 'flush-cache' command and try again."

	// If account was found, only the role was wrong
	if accountFound {
		return errors.New("Role '" + iamRole + "' was not found in account '" + accountNumber + "' - please double check you are using a role valid for this account." + commonPostfix)
	} else {
		return errors.New("Account number '" + accountNumber + "' could not be found - please double check account number." + commonPostfix)
	}

}

// Returns all the account information from kion including projects, which contain accounts, which contain roles
// This output is also sorted by usage frequency, most frequently used things on top
func (k Client) GetAllAccountInformation() ([]Project, error) {

	var projects []Project
	var accounts []Account
	var accessroles []AccessRole

	cache_key := cache.KEY_ACCOUNTS

	// If exists in the cache, return from there, otherwise fetch it fresh
	if k.NoReadCache || !k.Cache.ExistsString(cache_key) {

		if k.Debug {
			fmt.Println("Cache MISS for processed project list")
		}

		// Fan out and fetch projects, accounts, and roles all at once
		projects_chan := make(chan []Project)
		accounts_chan := make(chan []Account)
		accessroles_chan := make(chan []AccessRole)

		go func() {
			if k.Debug {
				fmt.Println("Fetching projects")
			}
			data, err := k.getProjects()
			if err != nil {
				panic(err)
			}
			if k.Debug {
				fmt.Println("Fetched projects")
			}
			projects_chan <- data
		}()

		go func() {
			if k.Debug {
				fmt.Println("Fetching accounts")
			}
			data, err := k.getAccounts()
			if err != nil {
				panic(err)
			}
			if k.Debug {
				fmt.Println("Fetched accounts")
			}
			accounts_chan <- data
		}()

		go func() {
			if k.Debug {
				fmt.Println("Fetching access roles")
			}
			data, err := k.getAccessRoles()
			if err != nil {
				panic(err)
			}
			if k.Debug {
				fmt.Println("Fetched access roles")
			}
			accessroles_chan <- data
		}()

		// Wait for all the data to be collected and returned by channels
		// Channels here block until all the data is returned
		projects = <-projects_chan
		accounts = <-accounts_chan
		accessroles = <-accessroles_chan

		if k.Debug {
			fmt.Println("Done fetching projects, accounts, and roles")
			fmt.Println("Now processing into a combine datastructure")
		}

		// Step 2 - combine projects, accounts, and access roles

		// Nest accounts under projects by project_id
		for a := 0; a < len(accounts); a++ {
			for _, accessrole := range accessroles {
				if accessrole.AccountID == accounts[a].ID {
					// Loop over access roles, if role has an account ID for
					// this project, add the account to the list of roles
					// under the project
					accounts[a].AccessRoles = append(accounts[a].AccessRoles, accessrole)
				}
			}
		}

		// Nest access roles under accounts by account_id
		for p := 0; p < len(projects); p++ {
			for _, account := range accounts {
				if account.ProjectID == projects[p].ID {
					// Loop over accounts, if account has a project ID for
					// this project, add the account to the list of accounts
					// under the project
					projects[p].Accounts = append(projects[p].Accounts, account)
				}
			}
		}

		// Now sort by stats
		projects = k.sortAccountInformationByStats(projects)

		// Convert to json to store in cache
		projects_json, err := json.Marshal(projects)
		if err != nil {
			log.Printf("Error: %s", err)
			return nil, err
		}

		k.Cache.SetString(cache_key, string(projects_json))

		// Fetch time used to refresh after a given time
		k.Cache.SetInt(cache_key+"_fetchtime", int(time.Now().Unix()))

		return projects, nil

	} else {
		if k.Debug {
			fmt.Println("Cache HIT for processed project list")
		}
		project_blob_raw, err := k.Cache.GetString(cache_key)
		if err != nil {
			return nil, err
		}

		var projects []Project
		err = json.Unmarshal([]byte(project_blob_raw), &projects)
		if err != nil {
			return nil, err
		}

		// Now sort by stats
		// NOTE - can't use the sorted version from cache, because stats update more than the cached account list
		projects = k.sortAccountInformationByStats(projects)

		return projects, nil
	}

	// Nest access role under accounts
}

func (k Client) getProjects() ([]Project, error) {

	cache_key := cache.KEY_PROJECTS_RAW

	// If exists in the cache, return from there, otherwise fetch it fresh
	if k.NoReadCache || !k.Cache.ExistsString(cache_key) {

		if k.Debug {
			fmt.Println("Cache MISS for projects")
		}

		accounts, err := k.ApiClient.GetProjects()
		if err != nil {
			return nil, err
		}

		acc_json, err := json.Marshal(accounts)
		if err != nil {
			log.Printf("Error: %s", err)
			return nil, err
		}

		k.Cache.SetString(cache_key, string(acc_json))
		// Fetch time used to refresh after a given time
		k.Cache.SetInt(cache_key+"_fetchtime", int(time.Now().Unix()))

		return accounts, nil

	} else {

		if k.Debug {
			fmt.Println("Cache HIT for projects")
		}
		project_blob_raw, err := k.Cache.GetString(cache_key)
		if err != nil {
			return nil, err
		}

		var projects []Project
		err = json.Unmarshal([]byte(project_blob_raw), &projects)
		if err != nil {
			return nil, err
		}

		return projects, nil

	}
}

func (k Client) getAccounts() ([]Account, error) {

	cache_key := cache.KEY_ACCOUNTS_RAW

	// If exists in the cache, return from there, otherwise fetch it fresh
	if k.NoReadCache || !k.Cache.ExistsString(cache_key) {

		if k.Debug {
			fmt.Println("Cache MISS for accounts")
		}

		accounts, err := k.ApiClient.GetAccounts()
		if err != nil {
			return nil, err
		}

		acc_json, err := json.Marshal(accounts)
		if err != nil {
			log.Printf("Error: %s", err)
			return nil, err
		}

		k.Cache.SetString(cache_key, string(acc_json))
		// Fetch time used to refresh after a given time
		k.Cache.SetInt(cache_key+"_fetchtime", int(time.Now().Unix()))

		return accounts, nil

	} else {

		if k.Debug {
			fmt.Println("Cache HIT for accounts")
		}
		accounts_blob_raw, err := k.Cache.GetString(cache_key)
		if err != nil {
			return nil, err
		}

		var accounts []Account
		err = json.Unmarshal([]byte(accounts_blob_raw), &accounts)
		if err != nil {
			return nil, err
		}

		return accounts, nil

	}

}

func (k Client) getAccessRoles() ([]AccessRole, error) {

	cache_key := cache.KEY_ROLES_RAW

	// If exists in the cache, return from there, otherwise fetch it fresh
	if k.NoReadCache || !k.Cache.ExistsString(cache_key) {

		if k.Debug {
			fmt.Println("Cache MISS for access roles")
		}

		accounts, err := k.ApiClient.GetAccessRoles()
		if err != nil {
			return nil, err
		}

		acc_json, err := json.Marshal(accounts)
		if err != nil {
			log.Printf("Error: %s", err)
			return nil, err
		}

		k.Cache.SetString(cache_key, string(acc_json))
		// Fetch time used to refresh after a given time
		k.Cache.SetInt(cache_key+"_fetchtime", int(time.Now().Unix()))

		return accounts, nil

	} else {

		if k.Debug {
			fmt.Println("Cache HIT for access roles")
		}
		accessroles_blob_raw, err := k.Cache.GetString(cache_key)
		if err != nil {
			return nil, err
		}

		var accessroles []AccessRole
		err = json.Unmarshal([]byte(accessroles_blob_raw), &accessroles)
		if err != nil {
			return nil, err
		}

		return accessroles, nil

	}

}

// Returns a project struct given a project name
func (k Client) GetProjectByProjectName(projectName string) Project {

	projects, _ := k.GetAllAccountInformation()
	for _, project := range projects {
		if project.Name == projectName {
			return project
		}
	}
	return Project{}
}

// Returns an account struct given an account name and a project struct
func (k Client) GetAccountByAccountName(project Project, accountName string) Account {

	for _, account := range project.Accounts {
		if account.Name == accountName {
			return account
		}
	}
	return Account{}
}

// Returns an account struct given an account number and a project struct
func (k Client) GetAccountByAccountNumber(accountNumber string) Account {

	projects, _ := k.GetAllAccountInformation()
	for _, project := range projects {
		for _, account := range project.Accounts {
			if account.Number == accountNumber {
				return account
			}
		}
	}
	return Account{}
}

// Returns a role struct given an account struct and a role name
func (k Client) GetRoleByRoleName(account Account, roleName string) AccessRole {

	for _, role := range account.AccessRoles {
		if role.Name == roleName {
			return role
		}
	}
	return AccessRole{}
}

// Returns a role struct given an account struct and a role name
func (k Client) GetRoleByIamRoleName(account Account, roleName string) AccessRole {

	for _, role := range account.AccessRoles {
		if role.AwsIamRoleName == roleName {
			return role
		}
	}
	return AccessRole{}
}

// TODO - consider adding error handlers to the functions above "GetXbyX"
