package api

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/url"

	"github.com/spf13/viper"
)

// credential response struct - only used to parse out the http response
type credentialResponse struct {
	Status int        `json:"status"`
	List   Credential `json:"data"`
}

// struct containing the actual credential
type Credential struct {
	AccessKey       string `json:"access_key"`
	SecretAccessKey string `json:"secret_access_key"`
	SessionToken    string `json:"session_token"`
}

type CredentialRequestBody struct {
	AccountNumber string `json:"account_number"`
	RoleName      string `json:"iam_role_name"`
}

// Get access roles
func (k Client) GetCredential(accountNumber string, roleName string) (Credential, error) {

	url := k.BaseUrl + "/api/v3/temporary-credentials"

	//Create user struct which need to post.
	reqBody := CredentialRequestBody{
		AccountNumber: accountNumber,
		RoleName:      roleName,
	}

	bodyRaw, _ := json.Marshal(reqBody)

	request, err := http.NewRequest("POST", url, bytes.NewBuffer(bodyRaw))
	if err != nil {
		return Credential{}, err
	}

	body, err := k.executeRequestAndCheckResponse(request)
	if err != nil {
		return Credential{}, err
	}

	var creds credentialResponse
	json.Unmarshal(body, &creds)

	return creds.List, nil

}

// Given a set of credentials, returns the AWS console access url
func (cred Credential) GetAwsConsoleUrl() (string, error) {

	var err error

	sessionDuration := viper.GetString("aws-console-session-timeout")

	// Build a struct that will be serialized to send to AWS
	credBundle := struct {
		AccessKey       string `json:"sessionId"`
		SecretAccessKey string `json:"sessionKey"`
		SessionToken    string `json:"sessionToken"`
	}{
		AccessKey:       cred.AccessKey,
		SecretAccessKey: cred.SecretAccessKey,
		SessionToken:    cred.SessionToken,
	}

	var credBundleString string
	var tmp []byte

	tmp, err = json.Marshal(credBundle)
	credBundleString = string(tmp)

	if err != nil {
		return "", err
	}

	// Build a URL to fetch sign-in token
	signinTokenUrl := "https://signin.aws.amazon.com/federation"
	signinTokenUrl += "?Action=getSigninToken"
	signinTokenUrl += "&SessionDuration=" + sessionDuration
	signinTokenUrl += "&Session=" + url.QueryEscape(credBundleString)

	// Use the signinTokenUrl to get a signinToken
	resp, err := http.Get(signinTokenUrl)
	if err != nil {
		log.Fatalln(err)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	// Parse out the signin token from the response, so the final login URL can be built
	signInToken := struct {
		SigninToken string `json:"SigninToken"`
	}{}
	json.Unmarshal(body, &signInToken)

	// Build the final URL - going to this URL will result in a logged in session
	// this should be sent to the user's browser tab and opened
	sessionLoginUrl := "https://signin.aws.amazon.com/federation"
	sessionLoginUrl += "?Action=login"
	sessionLoginUrl += "&Issuer=Example.org"
	sessionLoginUrl += "&Destination=" + url.QueryEscape("https://console.aws.amazon.com/")
	sessionLoginUrl += "&SigninToken=" + signInToken.SigninToken

	return sessionLoginUrl, nil

}
