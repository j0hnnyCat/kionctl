package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// getCredsCliCmd represents the getCredsCli command
var usageCliCmd = &cobra.Command{
	Use:   "usage",
	Short: "Prints more extensive usage instructions.",
	Long:  `Run this command without arguments to print common workflows.`,

	Run: func(cmd *cobra.Command, args []string) {

		// NOTE - If you update this, also update readme
		help := `
This CLI tool helps you to make easier use of Kion (formerly known as CloudTamer) at
the command line, without needing to open a browser.

## Connecting to an Account

Just run the "get-creds" command, and you will be prompted. See the repo readme for 
more information on non-interactive use.

$ kionctl get-creds

## Listing available accounts

To see what accounts are available, use the "list-accounts" command.

$ kionctl list-accounts

## Fetching Account Details

Due to Kion often returning account details slowly, kionctl makes use of caching of the
account information.

If you expect to see new account information and don't, you can do one of two things:

Fetch info - this fetches the latest account, project, and role list from Kion, and puts it
in the cache. This command is also something that might be useful to schedule in a cron-job
to run in the background every few days, to ensure the cached account info is always up
to date.

$ kionctl fetch-info

Flushing the cache - this clears all the caches, but does not reload them. They will be
reloaded the next time they are needed, leading to a slower execution. Generally, you don't
want to do this unless all else fails, as other useful data is also cached, like commonly
used accounts, among other things.

$ kionctl flush-cache
 `
		fmt.Println(help)

	},
}

func init() {

	rootCmd.AddCommand(usageCliCmd)
}
