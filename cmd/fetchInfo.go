package cmd

import (
	//"fmt"

	"log"
	"os"
	"strconv"

	"github.com/spf13/cobra"
)

// listAccountsCmd represents the listAccounts command
var fetchInfoCmd = &cobra.Command{
	Use:     "fetch-info",
	Example: "  kionctl fetch-info\n  kionctl fetch",
	Short:   "Fetch all account and role information from Kion",
	Long: `Due to high API latency of fetching, this CLI is written to operate primarily on cached data
around accounts, projects, and permissions.

This command fetches all the projects available to the current user, the accounts contained
in the projects, and the roles available on those accounts, and writes them to a local cache.

This should be rerun if your permissions have changed so that they can be reflected locally.
	`,
	Run: func(cmd *cobra.Command, args []string) {

		// TODO - add check of caching of account list, currently it's writing to cache but not reading
		// TODO - look at adding concurrent fetch of accounts and projects

		log.Println("Fetching account details...")

		// Fetch accounts, after having forced not to read from cache
		kionClient.NoReadCache = true
		projects, err := kionClient.GetAllAccountInformation()
		if err != nil {
			log.Println(err)
			os.Exit(1)
		}

		// intentionally doing nothing with the projects var, as this function's
		// primary purpose is warming local caches

		if kionClient.Debug {
			log.Println("Found " + strconv.Itoa(len(projects)) + " projects")
		}

	},
}

func init() {
	rootCmd.AddCommand(fetchInfoCmd)
}
