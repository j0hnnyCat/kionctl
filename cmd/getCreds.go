package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/benyanke/kionctl/browser"
	"gitlab.com/benyanke/kionctl/credfile"
	"gitlab.com/benyanke/kionctl/kion/aws"
	"gitlab.com/benyanke/kionctl/kion/prompt"
	"golang.org/x/exp/slices"
)

// getCredsCliCmd represents the getCredsCli command
var getCredsCliCmd = &cobra.Command{
	Use:     "get-creds",
	Aliases: []string{"creds", "get"},
	Short:   "Get AWS CLI credentials",
	Example: `
  # Non-interactive use, returns credentials immediately
      kionctl get-creds -a 345678901234 -r ct-examplecorp-developer -s none
      kionctl get-creds --account-number 345678901234 --role-name ct-examplecorp-developer --role-name-assume none

  # Interactive use, asks user for account and role information
      kionctl get-creds
	
  # Non-interactive advanced use, fetches credentials as 'ct-examplecorp-developer' then uses that
  # role to assume the 'ct-examplecorp-admin' role before returning credentials
      kionctl get-creds -a 345678901234 -r ct-examplecorp-developer -s ct-examplecorp-admin
      kionctl get-creds --account-number 345678901234 --role-name ct-examplecorp-developer --role-name-assume ct-examplecorp-admin`,
	Long: `Gets credentials for use at the CLI, IAC, or anywhere else amazon IAM credentials can be used.

You can either specify an account number and role, or not specify and be prompted for them interactively.

For more advanced usage instructions, run:
 kionctl usage

	`,

	Run: func(cmd *cobra.Command, args []string) {

		var account Account
		var role AccessRole
		var err error

		accountNumber := viper.GetString("account-number")
		roleName := viper.GetString("role-name")
		awsCredFilePath := viper.GetString("aws-credential-file-path")

		// Validate credential mode
		roleToAssume, _ := cmd.Flags().GetString("role-name-assume")
		credentialMode, _ := cmd.Flags().GetString("mode")
		validCredentialModes := []string{"file", "cli", "console"}

		if credentialMode == "prompt" {
			credentialMode, err = prompt.ForEnumString("Select a credential output mode", validCredentialModes)
			if err != nil {
				fmt.Println(err)
				os.Exit(2)
			}
		}

		if !slices.Contains(validCredentialModes, credentialMode) {
			fmt.Println("Invalid credential mode '" + credentialMode + "', must be one of: [" + strings.Join(validCredentialModes[:], ", ") + "]")
			os.Exit(1)
		}

		if accountNumber == "" {
			project, err := prompt.ForProject(kionClient)
			if err != nil {
				fmt.Println(err)
				os.Exit(2)
			}

			// Only prompt for account if there's multiples, otherwise just auto-select
			// the only account

			if len(project.Accounts) == 0 {
				fmt.Println("No accounts in this project - try another project or use the 'list-accounts' command")
				os.Exit(1)
			} else if len(project.Accounts) == 1 {
				account = project.Accounts[0]
				fmt.Print("   - only found one account within this project, selecting " + account.Name)
			} else {
				account, err = prompt.ForAccount(kionClient, project)
				if err != nil {
					fmt.Println(err)
					os.Exit(2)
				}
			}
		} else {
			account = kionClient.GetAccountByAccountNumber(accountNumber)
			if account.Number == "" {
				fmt.Println("Account not found")
				os.Exit(1)
			}
		}

		if len(account.AccessRoles) == 0 {
			fmt.Println("No access roles in the selected account - you might find the 'list-accounts' command useful to find another one")
			os.Exit(1)
		}

		// Get the list of roles able to be assumed for the current account
		assumableRoles := getAssumableRoles(account.Number)

		// Don't even prompt the user if there's no assumable roles configured, or it's been specified in a flag
		if (len(roleToAssume) == 0) && (len(assumableRoles) > 0) {
			fmt.Print("\n")
			noString := "no, use credentials as fetched"
			options := []string{noString}
			options = append(options, assumableRoles...)

			roleToAssume, err = prompt.ForEnumString("Do you want to assume a role with your fetched credentials?", options)
			if err != nil {
				fmt.Println(err)
				os.Exit(2)
			}

			if roleToAssume == noString {
				roleToAssume = ""
			}
		}

		// If the assume role string is one of the following 'ignore strings', then don't even try
		// to assume one. Useful for non-interactive use. Otherwise, go through the selection process
		// to find a role to assume
		if slices.Contains([]string{"none", "null", "na"}, roleToAssume) {
			roleToAssume = ""
		}

		if roleName == "" {
			if len(account.AccessRoles) == 1 {
				role = account.AccessRoles[0]
				fmt.Println("Found the access role within this account, selecting " + role.Name)
			} else {
				role, err = prompt.ForRole(kionClient, account)
				if err != nil {
					fmt.Println(err)
					os.Exit(2)
				}
			}

		} else {
			role = kionClient.GetRoleByIamRoleName(account, roleName)
			if (role == AccessRole{}) {
				fmt.Println("Access role not found")
				os.Exit(1)
			}
		}

		if kionClient.Debug {
			log.Println("Requesting IAM credentials for account number '" + accountNumber + "' with role '" + roleName + "'")
		}

		credential, err := kionClient.GetCredential(account.Number, role.AwsIamRoleName, credentialMode)

		if err != nil {
			log.Println(err)
			os.Exit(1)
		}

		// Use the credentials above to assume another role, if requested
		if len(roleToAssume) > 0 {
			credential, err = aws.AssumeNamedRole(credential, account.Number, roleToAssume)

			if err != nil {
				log.Println(err)
				os.Exit(1)
			}
		}

		if credentialMode == "cli" {

			fmt.Println("")
			fmt.Println("Credential environment variables:")
			fmt.Println("")
			fmt.Println("export AWS_ACCESS_KEY_ID='" + credential.AccessKey + "'")
			fmt.Println("export AWS_SECRET_ACCESS_KEY='" + credential.SecretAccessKey + "'")
			fmt.Println("export AWS_SESSION_TOKEN='" + credential.SessionToken + "'")
			fmt.Println("")
			fmt.Println("")

		} else if credentialMode == "console" {

			var consoleUrl string

			consoleUrl, err = credential.GetAwsConsoleUrl()

			if err != nil {
				log.Println(err)
				os.Exit(1)
			}
			fmt.Println()
			browser.OpenUrl(consoleUrl)

		} else if credentialMode == "file" {

			fmt.Println()

			credProfile := account.Name

			err := credfile.StoreCredentials(credential, credProfile, awsCredFilePath)

			if err != nil {
				log.Println(err)
				os.Exit(1)
			}

			fmt.Println("Credentials written to " + awsCredFilePath + " as credential profile " + credProfile)
			fmt.Println()

		} else {
			panic("Unexpected state")
		}

	},
}

// Given an account ID, this function returns the names of eligible roles to assume (as configured by
// the config options.
func getAssumableRoles(accountId string) []string {

	var assumableRoles []string

	allAssumableRoles := viper.GetStringMapStringSlice("aws-additional-roles")

	// If the global role list exists, add all it's entries to the output slice
	if val, ok := allAssumableRoles["global"]; ok {
		assumableRoles = append(assumableRoles, val...)
	}

	// If the account specific role list exists, add all it's entries to the output slice
	if val, ok := allAssumableRoles[accountId]; ok {
		assumableRoles = append(assumableRoles, val...)
	}

	return assumableRoles

}
func init() {

	// TODO - add completion to the flag values by parsing values from kionClient.GetAllAccountInformation() and passing to cobra
	// in the right way

	getCredsCliCmd.PersistentFlags().StringP("account-number", "a", "", "Account number to connect to")
	getCredsCliCmd.PersistentFlags().StringP("role-name", "r", "", "Role name to fetch credentials for")
	getCredsCliCmd.PersistentFlags().StringP("role-name-assume", "s", "", "Role to assume after fetching credentials as role-name. Set to 'none' to skip prompt")
	getCredsCliCmd.Flags().StringP("mode", "m", "prompt", "How to use credentials - [cli, console, file, prompt]")

	viper.BindPFlags(getCredsCliCmd.PersistentFlags())

	rootCmd.AddCommand(getCredsCliCmd)
}
