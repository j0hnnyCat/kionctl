package credfile

import (
	"errors"
	"log"
	"os"

	"gitlab.com/benyanke/kionctl/kion/api"
	"gopkg.in/ini.v1"
)

type Credential = api.Credential

// Stores aws crededentials in ~/.aws/credentials
func StoreCredentials(credential Credential, profileName string, credFilePath string) error {

	// Make empty file if it doesn't exist
	// so the logic below can assume files always exist
	if _, err := os.Stat(credFilePath); errors.Is(err, os.ErrNotExist) {
		emptyFile, err := os.Create(credFilePath)
		if err != nil {
			log.Fatal(err)
		}
		emptyFile.Close()

		err = os.Chmod(credFilePath, 0600)
		if err != nil {
			log.Println(err)
		}

	}

	cfg, err := ini.Load(credFilePath)
	if err != nil {
		return err
	}

	cfg.Section(profileName).Key("aws_access_key_id").SetValue(credential.AccessKey)
	cfg.Section(profileName).Key("aws_secret_access_key").SetValue(credential.SecretAccessKey)
	cfg.Section(profileName).Key("aws_session_token").SetValue(credential.SessionToken)
	cfg.SaveTo(credFilePath)

	return nil

}
