package browser

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
)

func OpenUrl(url string) {

	if os.Getenv("KIONCTL_OFFLINE_DEV") == "1" {
		fmt.Print("!! Replacing AWS login url with example.com because offline dev mode is activated !!\n\n")
		url = "https://example.com"
	}

	var err error
	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		fmt.Println("\nThis OS not supported for automatic URL opening. Please copy and paste the following")
		fmt.Println("URL into your browser:")
		fmt.Print("\n*************************************\n\n")
		fmt.Println(url)
		fmt.Print("\n*************************************\n\n")
		return
	}

	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Opening url in browser\n")
}
